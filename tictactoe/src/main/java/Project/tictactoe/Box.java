package Project.tictactoe;

import java.awt.Color;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author David Millard
 * 
 * This class creates the 3x3 grid for the game
 *
 */
public class Box extends JPanel{
	/**
     * Square graphic in the client window.  Each square is a white panel.  
     * Client calls setIcon() to fill it with an Icon (X or O).
     */
	private static final long serialVersionUID = 1L;
	JLabel label = new JLabel((Icon)null);

    public Box() {
        setBackground(Color.white);
        add(label);
    }

    public void setIcon(Icon icon) {
        label.setIcon(icon);
    }
}
