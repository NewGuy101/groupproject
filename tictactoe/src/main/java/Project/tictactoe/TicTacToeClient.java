package Project.tictactoe;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;




/**
 * @author David Millard
 * 
 * This class creates the clients for the game
 *
 */
public class TicTacToeClient {

    private JFrame jframe = new JFrame("Tic Tac Toe");
    private JLabel jlabel = new JLabel("");
    private ImageIcon playericon;
    private ImageIcon opponentIcon;

    private static int PORT = 3371;
    private Socket clientSocket;
    private BufferedReader sockIn;
    private PrintWriter sockOut;
    
    private Box[] board = new Box[9];
    private Box currentBox;
    
   
    /**
     * Constructs the client by connecting to a server and constructs the GUI
     * @param serverAddress
     * @throws Exception
     */
    public TicTacToeClient(String serverAddress) throws Exception {

       
    	clientSocket = new Socket(serverAddress, PORT);
    	sockIn = new BufferedReader(new InputStreamReader(
    			clientSocket.getInputStream()));
    	sockOut = new PrintWriter(clientSocket.getOutputStream(), true);

       
        jlabel.setBackground(Color.gray);
        jframe.getContentPane().add(jlabel, "South");

        JPanel boardPanel = new JPanel();
        boardPanel.setBackground(Color.black);
        boardPanel.setLayout(new GridLayout(3, 3, 2, 2));
        for (int i = 0; i < board.length; i++) {
            final int j = i;
            board[i] = new Box();
            board[i].addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    currentBox = board[j];
                    sockOut.println("MOVE " + j);
                }
            }
            );
            boardPanel.add(board[i]);
        }
        jframe.getContentPane().add(boardPanel, "Center");
    }
    private boolean wantsToPlayAgain() {
        int response = JOptionPane.showConfirmDialog(jframe,
            "Game Over",
            "Play again?",
            JOptionPane.YES_NO_OPTION);
        jframe.dispose();
        return response == JOptionPane.YES_OPTION;
    }
    /**
     * The client's main thread will listen for messages from the server. 
     * The first message will be the "WELCOME" message in which we receive our 
     * mark.  Then we listen for "VALID_MOVE", "OPPONENT_MOVED", "VICTORY", 
     * "DEFEAT", "TIE", "OPPONENT_QUIT or "MESSAGE" messages in a loop. Each 
     * message is handled accordingly. The "VICTORY", "DEFEAT" and "TIE" ask 
     * the user whether or not to play another game.  If no is selected, the 
     * loop is exited and the server is sent a "QUIT" message. If an 
     * OPPONENT_QUIT message is recevied then the loop will exit and the 
     * server will be sent a "QUIT" message also.
     */
    public void play() throws Exception {
        String message;
        try {
            message = sockIn.readLine();
            System.out.println("\""+message+"\"");
            if (message.startsWith("WELCOME")) {
                char mark = message.charAt(8);
            	playericon = new ImageIcon(getClass().getResource("/x.jpg"));
            	opponentIcon = new ImageIcon(getClass().getResource("/o.jpg"));
                jframe.setTitle("Tic Tac Toe - Player " + mark);
            }
            while (true) {
            	message = sockIn.readLine();
                System.out.println("\""+message+"\"");
                if (message.startsWith("VALID_MOVE")) {
                    jlabel.setText("Valid move, please wait");
                    currentBox.setIcon(playericon);
                    currentBox.repaint();
                } 
                else if (message.startsWith("OPPONENT_MOVED")) {
                    int loc = Integer.parseInt(message.substring(15));
                    board[loc].setIcon(opponentIcon);
                    board[loc].repaint();
                    jlabel.setText("Opponent moved, your turn");
                } 
                else if (message.startsWith("VICTORY")) {
                	jlabel.setText("You win!");
                    break;
                } 
                else if (message.startsWith("DEFEAT")) {
                	jlabel.setText("You lose.");
                    break;
                } 
                else if (message.startsWith("TIE")) {
                	jlabel.setText("You tied.");
                    break;
                } 
                else if (message.startsWith("MESSAGE")) {
                	jlabel.setText(message.substring(8));
                }
            }
            sockOut.println("QUIT");
        }
        finally {
            clientSocket.close();
        }
    }
    
    /**
     * Implements the play again method
     */
    public static void main(String[] args) throws Exception {
        while (true) {
            String serverAddress = (args.length == 0) ? "localhost" : args[1];
            TicTacToeClient client = new TicTacToeClient(serverAddress);
            client.jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            client.jframe.setSize(480, 320);
            client.jframe.setVisible(true);
            client.jframe.setResizable(false);
            client.play();
            if (!client.wantsToPlayAgain()) {
                break;
            }
        }
    }
}
