package Project.tictactoe;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


/**
 * @author David Millard
 * 
 * This class creates the players for the game
 *
 */
public class TicTacToePlayer extends Thread{
        char xo;
        TicTacToePlayer opponent;
        Socket socket;
        BufferedReader sockIn;
        PrintWriter sockOut;
        TicTacToeProtocol tp;
	/**
     * Construct handler for a @param socket and @param xo.
     * Initializes the streams, displays the first two welcoming messages.
     */
    public TicTacToePlayer(Socket socket, char xo,TicTacToeProtocol game) {
        this.socket = socket;
        this.xo = xo;
        this.tp=game;
        try {
        	sockIn = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        	sockOut = new PrintWriter(socket.getOutputStream(), true);
        	sockOut.println("WELCOME Hello there " + xo);
        	sockOut.println("MESSAGE Waiting for opponent to connect");
        } catch (IOException e) {
            System.out.println("Player has encountered IOException: " + e);
        }
    }


    /**
     * Message for the opponent's move using @param location.
     */
    public void opponentMoved(int location) {
    	sockOut.println("OPPONENT_MOVED " + location);
    	sockOut.println(
                tp.winner() ? "DEFEAT" : tp.boardFilledUp() ? "TIE" : "");
    }
    /**
     * Set the opponent using @param opponent.
     */
    public void setOpponent(TicTacToePlayer opponent) {
        this.opponent = opponent;
    }
    /**
     * The run method of this thread.
     */
    public void run() {
        try {
            // The thread is only started after everyone connects.
        	sockOut.println("MESSAGE Both players connected");

            // Tell the first player to go.
            if (xo == 'X') {
            	sockOut.println("MESSAGE Your move");
            }

            while (true) {
                String command = sockIn.readLine();
                System.out.println("\""+command+"\"");
                if (command.startsWith("MOVE")) {
                    int location = Integer.parseInt(command.substring(5));
                    if (tp.legalMove(location, this)) {
                    	sockOut.println("VALID_MOVE");
                    	sockOut.println(tp.winner() ? "VICTORY"
                                     : tp.boardFilledUp() ? "TIE"
                                     : "");
                    }
                	else {
                		sockOut.println("MESSAGE ?");
                    }
                    
                } 
                else if (command.startsWith("QUIT")) {
                    return;
                }
            }
        } 
        catch (IOException e) {
            System.out.println("Player has encountered IOException: " + e);
        } 
        finally {
            try {socket.close();} catch (IOException e) {}
        }
    }
}
