package Project.tictactoe;


/**
 * @author David Millard
 * 
 * This class creates the protocols for the game
 *
 */
public class TicTacToeProtocol {
	/**
     * A board has nine squares and Each square is either empty or it is owned 
     * by a player.  Using an array of player references.  If null, 
     * the corresponding square is empty, otherwise the array cell stores a 
     * reference to the player that owns it.
     */
    private TicTacToePlayer[] board = {
        null, null, null,
        null, null, null,
        null, null, null
    };

    /**
     * The current player.
     */
    TicTacToePlayer player;
    /**
     * @return whether or not a player has won
     */
    public boolean winner() {
        return
            (board[0] != null && board[0] == board[1] && board[0] == board[2])||
          (board[3] != null && board[3] == board[4] && board[3] == board[5])||
          (board[6] != null && board[6] == board[7] && board[6] == board[8])||
          (board[0] != null && board[0] == board[3] && board[0] == board[6])||
          (board[1] != null && board[1] == board[4] && board[1] == board[7])||
          (board[2] != null && board[2] == board[5] && board[2] == board[8])||
          (board[0] != null && board[0] == board[4] && board[0] == board[8])||
          (board[2] != null && board[2] == board[4] && board[2] == board[6]);
    }

    /**
     * @return whether there are no more empty squares.
     */
    public boolean boardFilledUp() {
        for (int i = 0; i < board.length; i++) {
            if (board[i] == null) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Called by the player threads whenever a move is made. This method 
     * checks to see if the move is legal: the player that moves must be 
     * the current player and the square in which the player moves must be 
     * empty.  If the move is legal the game state is updated and the other 
     * player is notified of the move so its client will update.
     * @param location
     * @param lmPlayer
     * @return
     */
    public synchronized boolean legalMove(int location, TicTacToePlayer lmPlayer) {
        if (lmPlayer == player && board[location] == null) {
            board[location] = player;
            player = player.opponent;
            player.opponentMoved(location);
            return true;
        }
        return false;
    }
}
