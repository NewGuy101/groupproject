package Project.tictactoe;

import java.net.ServerSocket;

/**
 * @author David Millard
 * 
 * This class creates the server for the game
 *
 */
public class TicTacToeServer {
    /**
     * Runs the application and pairs up clients that connect.
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ServerSocket listener = new ServerSocket(3371);
        System.out.println("Tic Tac Toe Server is Running");
        try {
            while (true) {
            	TicTacToeProtocol game = new TicTacToeProtocol();
            	TicTacToePlayer playerX = new TicTacToePlayer(listener.accept(), 'X',game);
                TicTacToePlayer playerO = new TicTacToePlayer(listener.accept(), 'O',game);
                playerX.setOpponent(playerO);
                playerO.setOpponent(playerX);
                game.player = playerX;
                playerX.start();
                playerO.start();
            }
        } 
        finally {
            listener.close();
        }
    }
}
