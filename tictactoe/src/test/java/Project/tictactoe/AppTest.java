package Project.tictactoe;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.ServerSocket;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author David Millard
 * 
 * This class was for the testing of the project
 *
 */
public class AppTest{
	protected TicTacToeProtocol proto;
	protected TicTacToePlayer play1;
	protected TicTacToePlayer play2;
	protected TicTacToeClient cli;
	protected TicTacToeServer serve;
	protected ServerSocket listener;
	
	@BeforeEach
    void init() throws Exception {
		listener = new ServerSocket(3371);
		serve = new TicTacToeServer();
		cli= new TicTacToeClient("3371");
		proto = new TicTacToeProtocol();
		play1 = new TicTacToePlayer(listener.accept(), 'X', proto);
		play2 = new TicTacToePlayer(listener.accept(), 'O', proto);
	}
	@AfterEach
	void des() throws IOException {
		listener.close();
	}
	
	
}
